# P1.3 → V3 JavaScript na strani odjemalca

V pripravah na vaje V3 bo predstavljen kratek uvod v:

* **JavaScript** (načini vpeljave JavaScript kode, spremenljivke, logične operacije, vejitve, zanke, funkcije, objekti, seznami, DOM, dogodki in povratni klici, AJAX, JSON, kontekst, čakanje, da se stran naloži).

Omenjena vsebina nadgrajuje vsebino priprav na vaje V2, kjer je bil predstavljen kratek uvod v:

* **HTML** (prosto besedilo, oznake, atributi in elementi, naslov strani, odstavki, naslovi, seznami, povezave, slike, tabele, obrazci, SPAN in DIV, META oznake, napredne tabele),
* **CSS** (načini vpeljave slogov, selektorji, lastnosti in vrednosti, CLASS in ID selektorji, grupiranje in gnezdenje, psevdo razredi).